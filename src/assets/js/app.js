import $ from 'jquery';
import 'what-input';

// Foundation JS relies on a global variable. In ES6, all imports are hoisted
// to the top of the file so if we used `import` to import Foundation,
// it would execute earlier than we have assigned the global variable.
// This is why we have to use CommonJS require() here since it doesn't
// have the hoisting behavior.
window.jQuery = $;
require('foundation-sites');

// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
import './lib/foundation-explicit-pieces';

// Importing fullpage.js
import fullpage from 'fullpage.js';
import 'fullpage.js/dist/fullpage.css';

// import Swiper JS
// core version + navigation, pagination modules:
import Swiper, {
    EffectFade,
    Mousewheel,
    Thumbs
} from 'swiper';

// configure Swiper to use modules
Swiper.use([EffectFade, Thumbs, Mousewheel]);

$(document).foundation();

const swiperThumbs = new Swiper('.swiper-container-gifts-thumbs', {
    centeredSlides: true,
    centeredSlidesBounds: true,
    watchOverflow: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    direction: 'horizontal',
    slidesPerView: 'auto',

    //Breakpoints
    breakpoints: {
        1200: {
            direction: 'vertical',
            slidesPerView: 10,
        }
    },

});

const swiper = new Swiper('.swiper-container-gifts', {
    direction: 'horizontal',
    grabCursor: true,
    thumbs: {
        swiper: swiperThumbs
    },
    effect: 'fade',
    fadeEffect: {
        crossFade: true
    },
    mousewheel: true,
    watchSlidesProgress: true,
    virtualTranslate: true,
    watchSlidesVisibility: true,
});

// Initializing fullpage
var fullPageInstance = new fullpage('#full-page', {
    navigation: false,
    anchors: ['wellcome', 'intro', 'stories'],
    licenseKey: '01FF6715-D0E5410E-91461212-2DB0A111',
    responsiveWidth: 1200,
    normalScrollElements: '#stories-section'
});
